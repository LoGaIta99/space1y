function dy = ode_harmonic_oscill( t, y, omega0, gamma )

%ode_harmonic_oscill ODE system for the damped harmonic oscillator %
% PROTOTYPE
% dy = ode_harmonic_oscill( t, y, omega0, gamma )
%
% INPUT:
% t[1]          Time (can be omitted, as the system is autonomous)[T]
% y[2x1]        State of the oscillator (position and velocity) [ L, L/T ]
% omega0[1]     Natural frequency of the undamped oscillator [1/T]
% gamma[1]      Damping coefficient [1/T]
%
% OUTPUT:
%   dy[2x1]
%
% CONTRIBUTORS:
%   Massimo Piazza
%
% VERSIONS
%   2018-09-28: 1.0
%

% Set the derivatives of the state
dy = [             y(2);
      -2*gamma*y(2) - omega0^2*y(1)  ];

end


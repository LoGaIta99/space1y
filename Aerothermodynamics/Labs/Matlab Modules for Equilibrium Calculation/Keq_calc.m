function [Keq, dKeq_dT] = Keq_calc(Tin)

%air_phys_properties;

T  =  Tin;

T_inv = 1/T;

%  --- equilibrium constants (Park '90) and temperature derivatives evaluation

z    = 1.d+04 * T_inv;
par1 = 1.d+00 / z;
par2 = log( z );
par3 = z^2;

k(1) = exp(.50989d0 * par1 + 2.4773d0 + 1.7132d0 * par2 - 6.5441d0 * z + .029591d0 * par3 );
k(2) = exp(1.4766d0 * par1 + 1.6291d0 + 1.2153d0 * par2 - 11.457d0 * z - .009444d0 * par3 );
k(3) = exp(.50765d0 * par1 + .73575d0 + .48042d0 * par2 - 7.4979d0 * z - .016247d0 * par3 );

dk(1) = -k(1)*(-.50989d0 * par1 + 1.7132d0 - 6.5441d0 * z + .059182d0 * par3 ) * T_inv;
dk(2) = -k(2)*(-1.4766d0 * par1 + 1.2153d0 - 11.457d0 * z - .018888d0 * par3 ) * T_inv;
dk(3) = -k(3)*(-.50765d0 * par1 + .48042d0 - 7.4979d0 * z - .032494d0 * par3 ) * T_inv;

%  --- equilibrium constants for the implemented reactions


Keq(1) = sqrt(1.d+06 * k(1));

dKeq_dT(1) = 0.5 * 1.d+06 * dk(1) / sqrt(1.d+06 * k(1));

Keq(2) = sqrt(1.d+06 * k(2));

dKeq_dT(2) = 0.5 * 1.d+06 * dk(2) / sqrt(1.d+06 * k(2));

Keq(3) = sqrt(k(1) * k(2)) / k(3);

dKeq_dT(3) = 0.5 * ( dk(1) / k(1) + dk(2) / k(2) - 2 * dk(3) /k(3) );
dKeq_dT(3) = Keq(3) * dKeq_dT(3);


return
function landing_template
close all
orig_state = warning;

% ---------------------------- CONSTANTS DEFINITIONS ---------------------------

% ...


%NUMERICAL INTEGRATION OPTIONS
odeopt  = odeset('Events',@events,'RelTol',1e-5);   %ode45 options
% ...


% --------------------------- MULTISTART OPTIMIZATION --------------------------

ptmatrix = ...                              %Custom initial points
stpoints = CustomStartPointSet(ptmatrix);   %Starting points object creation

%fminunc (local optimizer) options
fminopt = optimoptions(@fminunc,...
                       'Display', 'none',...
                       'TolFun', 1e-6,...
                       'TolX', 1e-9,...
                       'Algorithm', 'quasi-newton',...
                       'maxFunEvals', 500);
%Optimizazion problem
problem = createOptimProblem('fminunc',...
                             'objective',@(x)objective(x),...
                             'x0', [0.01, -0.01],...
                             'options', fminopt);
%Multistart object
ms = MultiStart('Display','iter');
            
x = run(ms,problem,stpoints);   %SOLVE OPTIMIZATION


% --------------------------------- VIEW RESULTS -------------------------------

% ...





warning(orig_state);

% ----------------------------- AUXILIARY FUNCTIONS ----------------------------

    function f = objective(x)
    % Optimization Objective Function
        
        % ...
        
        [~,y] = ode45(@landingSystem, .. , odeopt); %LANDING NUMERICAL INTEGRATION
        
        % ...
    end

    function dy = landingSystem(t,y)
    end

    function [value,isterminal,direction] = events(t,y)
    % Numerical integration STOPPING FUNCTION
    % Integration is stopped as the lander reaches the target altitude.
    end

end


% LU backsubstitution
function [rhs]=lubksn(fn,fjac,indx,N_var)
 
  rhs = fn;
  
     ii = 0;
     for i = 1:N_var;

       k      = indx(i);
       sum    = rhs(k);
       rhs(k) = rhs(i);

       if( ii ~= 0 )

         for j = ii:i-1;
           sum = sum - fjac(i,j) * rhs(j);
         end

       elseif( sum ~= 0. )

         ii = i;

       end

       rhs(i) = sum;

     end

     for i = N_var:-1:1;

       sum = rhs(i);

       if( i < N_var )

         for j = i+1:N_var;
           sum = sum - fjac(i,j) * rhs(j);
         end
       end

       rhs(i) = sum / fjac(i,i);

     end

return
    


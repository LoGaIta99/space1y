clear all ;

d=0.05;
g=4/3;
P1=2;
P2=1;
f=0.003;
L=30;
D=0.05;

syms M1 M2 ;

eq1 = (4*f*L)/D == ((1-M1^2)/(g*M1^2))+
    ((g+1)/(2*g))*log(((g+1)*M1^2)/(2*(1+((g-1)/(2))*M1^2)))
    -((1-M2^2)/(g*M2^2))
    -((g+1)/(2*g))*log(((g+1)*M2^2)/(2*(1+((g-1)/(2))*M2^2)));
    
eq2 = P1/P2 == (M2/M1)*sqrt((2+(g-1)*M2^2)/(2+(g-1)*M1^2));

S = solve([eq1,eq2],[M1,M2]);
S.M1
S.M2

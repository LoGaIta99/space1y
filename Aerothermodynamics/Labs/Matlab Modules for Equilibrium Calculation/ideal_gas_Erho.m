function [T,P,P_rho,P_E,e_sp,h_sp,s_speed,rho_sp]=ideal_gas_Erho(E,rho)

air_phys_properties;

    rho_sp = [eta_O2_std*mol_mass(1)*rho eta_N2_std*mol_mass(2)*rho 0 0 0];

    R = (eta_O2_std + eta_N2_std) * Ru;

    P       = E * (gam - 1.)
    T       = P /( rho * R );
    P_rho   = 0.;
    P_E     = gam - 1.;
    e_sp    = gam * E / rho;
    h_sp    = e_sp + P/rho;
    s_speed = sqrt( gam * R * T );

return
clear

sigma=5.67e-8;
eps1=0.8;
eps2=0.25;
eps3=0.25;
alpha1=eps1;
alpha2=eps2;
A1=1;
A2=1;
A3=1;

V1=1*0.005; %A=1,thickness 5 mm
V2=1*0.005;
V3=1*0.005;

%densities and specific heats, aluminium
rho1=2700;
rho2=2700;
rho3=2700;

cv1=900;
cv2=900;
cv3=900;


%case 2 or 4 of Table 10.2 p. 545 L&L 4th ed.
F12=1-sin((pi/3)/2);
F13=F12;
F21=F12;
F23=F12;
F31=F12;
F32=F12;
%F31=(A3+A1-A2)/(2*A3);

R1=(1-eps1)/(eps1*A1);
R2=(1-eps2)/(eps2*A2);
R3=(1-eps3)/(eps3*A3);

R12=1/(A1*F12);
R13=1/(A1*F13);
R21=1/(A2*F21);
R23=1/(A2*F23);
R31=1/(A3*F31);
R32=1/(A3*F32);


%creation of the coefficient matrix
%3 unknowns: J1,J2,J3
CM=zeros(3,3);

%1st equation
CM(1,1)=(1/R1+1/R12+1/R13);
CM(1,2)=-1/R12;
CM(1,3)=-1/R13;

%2nd equation
CM(2,1)=-1/R21;
CM(2,2)=(1/R2+1/R21+1/R23);
CM(2,3)=-1/R23;

%3rd equation
CM(3,1)=-1/R31;
CM(3,2)=-1/R32;
CM(3,3)=(1/R3+1/R31+1/R32);


%forward FD, explicit method
%the coefficient matrix does not change in time so it can remain outside of
%the time-marching for
%the known terms vector on the contrary must be inside because it depends
%on the Ts

Dtau=1;

num_steps=36000;

%allocation of memory
T1=zeros(num_steps,1);
T2=zeros(num_steps,1);
T3=zeros(num_steps,1);
tau=zeros(num_steps,1);

%initial Ts
T1(1)=278;
T2(1)=278;
T3(1)=278;
tau(1)=0;

Ep2_sun=1350;
Ep2_alb=0.3*1350;
Ep2_ir=240;

for i=1:num_steps
    %creation of the vector of the known terms
    KT=[sigma*T1(i)^4/R1; sigma*T2(i)^4/R2; sigma*T3(i)^4/R3];

    %solution of the system
    J=CM\KT;

    %net energy exchanged by surface-2-surface radiation inside the satellite
    Epnet1=(J(1)-sigma*T1(i)^4)/R1;
    Epnet2=(J(2)-sigma*T2(i)^4)/R2;
    Epnet3=(J(3)-sigma*T3(i)^4)/R3;

    %energy balances on the three surfaces
    Qp1=(alpha1*Ep2_sun-eps1*sigma*T1(i)^4+Epnet1)*A1;
    Qp2=(alpha2*(Ep2_alb+Ep2_ir)-eps2*sigma*T2(i)^4+Epnet2)*A2;
    Qp3=(-eps3*sigma*T3(i)^4+Epnet3)*A3;

    %being: rho*V*cv*dT/dtau=Qp => T(i+1)=T(i)+Qp/(rho*V*cv)*Dtau
    T1(i+1)=T1(i)+Qp1/(rho1*V1*cv1)*Dtau;
    T2(i+1)=T2(i)+Qp2/(rho2*V2*cv2)*Dtau;
    T3(i+1)=T3(i)+Qp3/(rho3*V3*cv3)*Dtau;
    
    tau(i+1)=i*Dtau;
end

disp(sprintf('T1 = %.2f K',T1(i)));
disp(sprintf('T2 = %.2f K',T2(i)));
disp(sprintf('T3 = %.2f K',T3(i)));

figure; hold on;
plot(tau,T1,'r-');
plot(tau,T2,'g-');
plot(tau,T3,'b-');

%SALVATAGGIO RISULTATI
% vars=who;
% save('dummy_workspace_saving.mat');
% M_save_results_Latex_table4C(vars,'results_esX.txt','%.2f','dummy_workspace_saving.mat');
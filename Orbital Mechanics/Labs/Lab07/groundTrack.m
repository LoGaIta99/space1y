function [alpha, delta, lon, lat, T,Y] = groundTrack(rr0, vv0,   greenwich0,   n_orb,  mu, perturbations, perturbMethod)

% perturbations = [0or1 0or[C_r A2m]] vetor indicating on/off respectively for: [J2 SRP]

w_E = 2*pi/(23*3600 + 56*60 + 4.09);

[a,e,i,OM,om,theta] = car2kep(rr0,vv0,   mu);

pertJ2  = perturbations{1};
pertSRP = perturbations{2};

if ~pertJ2 & ~pertSRP
    isPerturbedMotion = 0;
else
    isPerturbedMotion = 1;
end 
    

% Initial conditions
if (strcmp(perturbMethod, 'cart')) || ~isPerturbedMotion || (nargin < 6)
    y0 = [rr0(:); vv0(:)];
elseif strcmp(perturbMethod, 'gauss')
    [a,e,i,OM,om,theta] = car2kep(rr0, vv0,   mu);
    y0 = [a,e,i,OM,om,theta]';
else
	error(sprintf('Method: %s NOT FOUND!', method))
end

% Integration timespan
T = 2*pi*sqrt(a^3/mu);
N_step = 5000*n_orb; % i.e. dt = approx. 10 seconds for a=30000km
N_step = N_step - mod(N_step, n_orb);
t_span = linspace(0,n_orb*T, N_step);

% Set integration options
options = odeset( 'RelTol', 1e-13, 'AbsTol', 1e-14 );


% Perform the integration
if isPerturbedMotion
    [T, Y] = ode113( @(t,y) ode_2bodyPerturb(t,y, mu, perturbations, perturbMethod), t_span, y0, options);
    switch perturbMethod
        case 'cart'
            rr_mat = Y(:,1:3);
        case 'gauss'
            for k = 1:size(Y,1);
                [rr, ~] = kep2car(Y(k,1), Y(k,2), Y(k,3), Y(k,4), Y(k,5), Y(k,6),   mu);
                rr_mat(k,:) = rr(:)';
                % vv_mat(k,:) = vv(:)';
            end
            
        otherwise
    end
        
else
    [T, Y] = ode113( @(t,y) ode_2body(t,y, mu), t_span, y0, options );
    rr_mat = Y(:,1:3);
end


[alpha, delta, l, m, n] = car2RA_Dec(rr_mat);

thetaG_vect = wrapTo2Pi( greenwich0 - w_E*t_span );

lon = rad2deg( wrapTo2Pi( atan2(m,l) + thetaG_vect' )  -  pi) ; % vector of longitudes

lat = rad2deg( delta ); % vector of latitudes



%%  PLOT

    h = figure('Name','Ground Track');
    set(h, 'Units', 'Normalized', 'OuterPosition', [.15 .25 .7 .7]);
    hold on;
    axis equal;
    set(gca,'XTick',[-180:15:180],'XTickMode','manual');
    set(gca,'YTick',[-90:10:90],'YTickMode','manual');
    xlim([-180,180]); ylim([-90,90]);
        

    image_file = 'earth.jpg';
    cdata      = flip(imread(image_file));
    imagesc([-180,180],[-90, 90],cdata);

    
    plot(lon, lat, '.r');
    xlabel('Longitude $\lambda$ [deg]')
    ylabel('Latitude $\phi$ [deg]')



end


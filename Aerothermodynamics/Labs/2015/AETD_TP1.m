clear all;
% aL = input('aL : ');
% bL = input('bL : ');
% cL = input('cL : ');
% dL = input('dL : ');
% aR = input('aR : ');
% bR = input('bR : ');
% cR = input('cR : ');
% dR = input('dR : ');
% xT = input('xT : ');
% xE = input('xE : ');
aL = 2.;
bL = -3.;
cL = 0.;
dL = 2.0;
aR = -3/8 ;
bR = 9/4 ;
cR = -27/8;
dR = 2.5 ;
xT = 1.0;
xE = 3.0 ;

P0 = input('P0 : ');
T0 = input('T0 : ');

gamma = 1.4
Patm = 1;

N = 100; % nombre de points de la discr�tisation spatiale

%AL polynomes � gauche (x entre 0 et xT) et � droite (x entre xT et xE)
AL = [aL bL cL dL];
AR = [aR bR cR dR];
%Ke : Ae/A*
Ke = polyval(AR,xE)/polyval(AR,xT);
%f : fonction A/A* = f(M)
f = @(M) (1/M)*((2/(gamma+1))*(1+((gamma-1)/2)*M^2))^((gamma+1)/(2*(gamma-1)))-Ke ;

% MeA : nombre de mach en sortie subsonique, en isentropique

% MeB : nombre de mach en sortie supersonique, en isentropique
MeA = fzero(f,[0.001 0.999]) ;
MeB = fzero(f,[1.001 20]) ;
%MeC nombre de mach en sortie si choc � la sortie
MeC = sqrt((1+((gamma-1)/2)*MeB^2)/(gamma*MeB^2-(gamma-1)/2));

PeA = P0*(1+(gamma-1)/2*MeA^2)^(-gamma/(gamma-1)) ;
PeB = P0*(1+(gamma-1)/2*MeB^2)^(-gamma/(gamma-1)) ;
PeC = PeB*(1+2*gamma/(gamma+1)*(MeB^2-1)) ;

%calcul de P et M en tout point
Mx = zeros(1,N);
Px = zeros(1,N);
x = zeros(1,N);

%cas subonique :
if Patm >PeA
    g = @(M) (1+(gamma-1)/2*M^2)^(gamma/(gamma-1))-P0/Patm ;
    MeSubs = fzero(g,1);
    
    KeSubs = (1/MeSubs)*((2/(gamma+1))*(1+((gamma-1)/2)*MeSubs^2))^((gamma+1)/(2*(gamma-1))) ;
    AstarSubs = polyval (AR , xE)/KeSubs ; 
    
    for ii = 1:N
        x(ii) = xE*ii/N ; 
        if x(ii) < xT
            Kx = (polyval (AL , x(ii)))/AstarSubs ; 
        else
            Kx = (polyval (AR , x(ii)))/AstarSubs ;
        end
        h1 = @(M) (1/M)*((2/(gamma+1))*(1+((gamma-1)/2)*M^2))^((gamma+1)/(2*(gamma-1)))-Kx ;
        Mx(ii)  = fzero(h1 ,[0.001 0.999]);
        Px(ii) = P0 * (1+(gamma-1)/2*Mx(ii)^2)^(-gamma/gamma-1) ; 
        
    end
elseif (PeC<Patm)&& (Patm<PeA)
        MeSuper = sqrt((sqrt(1+2*(gamma-1)*((P0*polyval(AL,xT)/(Patm*polyval(AR,xE)))^2*(2/(gamma+1))^((gamma+1)/(gamma-1))))-1)/(gamma-1));
        P02 = Patm*(1+(gamma-1)/2*MeSuper^2)^(gamma/(gamma-1)) ;
        
        
        h2 = @(M) ((gamma+1)/2*M^2/(1+(gamma-1)/2*M^2))^(gamma/(gamma-1))*(1/(2*gamma/(gamma+1)*M^2-(gamma-1)/(gamma+1)))^(1/(gamma-1))-P02/P0 ;
        Mshock1 = fzero(h2,[1.001 20]) ;
        
        Kshock = (1/Mshock1)*((2/(gamma+1))*(1+((gamma-1)/2)*Mshock1^2))^((gamma+1)/(2*(gamma-1)));
        k = @(x) polyval(AR , x)/polyval(AR , xT) - Kshock;
        
        Xshock = fzero(k,[xT xE]);
        
        %calcul de A* apr�s le choc
        KeSuper = (1/MeSuper)*((2/(gamma+1))*(1+((gamma-1)/2)*MeSuper^2))^((gamma+1)/(2*(gamma-1))) ;
        AstarSuper2 = polyval (AR , xE)/KeSuper ;
        for ii=1:N
            x(ii) = xE*ii/N ;
            
            if x(ii) < xT
                Kx = (polyval (AL , x(ii)))/polyval(AR, xT) ;
                l = @(M) (1/M)*((2/(gamma+1))*(1+((gamma-1)/2)*M^2))^((gamma+1)/(2*(gamma-1)))-Kx ;
                Mx(ii)  = fzero(l ,[0.001 0.999]);
                Px(ii) = P0 * (1+(gamma-1)/2*Mx(ii)^2)^(-gamma/gamma-1) ;
            elseif x(ii)< Xshock
                Kx = (polyval (AR , x(ii)))/polyval(AL, xT) ;
                l = @(M) (1/M)*((2/(gamma+1))*(1+((gamma-1)/2)*M^2))^((gamma+1)/(2*(gamma-1)))-Kx ;
                Mx(ii)  = fzero(l ,[1.001 20]);
                Px(ii) = P0 * (1+(gamma-1)/2*Mx(ii)^2)^(-gamma/gamma-1) ;
            else
                Kx = (polyval (AR , x(ii)))/AstarSuper2 ;
                l = @(M) (1/M)*((2/(gamma+1))*(1+((gamma-1)/2)*M^2))^((gamma+1)/(2*(gamma-1)))-Kx ;
                Mx(ii)  = fzero(l ,[0.001 0.999]);
                Px(ii) = P02 * (1+(gamma-1)/2*Mx(ii)^2)^(-gamma/gamma-1) ;
            end
                
         end
            
            
            
else 
    display('This case is not taken into account in this study')
end




figure (1)
plot(x,Px)
xlabel('x');
ylabel('Pressure');

figure (2)
plot(x,Mx)
xlabel('x');
ylabel('Mach Number');
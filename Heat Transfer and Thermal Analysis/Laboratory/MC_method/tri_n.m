%generate the normal direction vector of a triangle
%author: M. Guilizzoni, Department of Energy, Politecnico di Milano
%March 2014
%
function n_di=tri_n(tv1,tv2,tv3,unit)

n_di=cross(tv2-tv1,tv3-tv1);

%if unit = 1 => normalize normal vector
if unit==1
   mag=(sum(n_di.^2,2))^0.5;
   if not(mag==0)
      n_di=n_di/mag;
   end
end

end
%view factor calculation by [very basic] Monte Carlo method for two
%triangles
%author: M. Guilizzoni, Department of Energy, Politecnico di Milano
%March 2014
%
clear

%starts the time counter
tic

%choose the number of points and directions
num_points=2000;
num_dir=20;

%choose the vertices of the source (s) and target (t) triangles
s_tv1=[0,0,0];
s_tv2=[5,0,0];
s_tv3=[5,5,0];

t_tv1=[0,0,2];
t_tv2=[10,0,2];
t_tv3=[10,10,2];

%normal vector of the triangle, normalized
%s_n_di=tri_n(s_tv1,s_tv2,s_tv3,1);

s_tri=[s_tv1;s_tv2;s_tv3];
t_tri=[t_tv1;t_tv2;t_tv3];

figure;
hold on;
grid on;

%draw the triangles
trisurf([1 2 3],s_tri(:,1),s_tri(:,2),s_tri(:,3),'FaceColor','red','EdgeColor','none');
trisurf([1 2 3],t_tri(:,1),t_tri(:,2),t_tri(:,3),'FaceColor','green','EdgeColor','none');

%extract the random points in the source triangle
points=generate_points_in_triangle_fun(s_tv1,s_tv2,s_tv3,num_points,0);

%draw the source points
scatter3(points(:,1),points(:,2),points(:,3), 'b.');

%format the figure
view(60,30);
alpha(0.75);
axis equal;
xlabel('x');
ylabel('y');
zlabel('z');

%initializa the intersection counter and the intersection coordinate matrix
found=zeros(num_points,num_dir);
int_xyz=zeros(num_points,num_dir,3);

%for each source point cast num_dir random rays and determine if they
%intersect the target triangle
for p=1:num_points
    for d=1:num_dir
        %generate a random direction
        s_r_di=random_direction(1);
        %verify if the ray intersect the target triangle
        [found(p,d),int_xyz(p,d,:)]=int_ray_tri(points(p,:),s_r_di,t_tv1,t_tv2,t_tv3);  
        %draw the rays and the intersection points
        quiver3(points(p,1),points(p,2),points(p,3),s_r_di(1),s_r_di(2),s_r_di(3),'b-');
        scatter3(int_xyz(p,d,1),int_xyz(p,d,2),int_xyz(p,d,3),'k.');
    end
    p
end

%determine the view factor as the fraction of rays that reached the target
num_int=sum(sum(found));
frac_int=num_int/(num_points*num_dir);

%output the elapsed time
toc
%%%%%%%%%%%%%-> air_phys_properties <-%%%%%%%%%%%%%%%%%
%% SET constant
%....... Molar mass
   mol_mass(1)  = 3.19988e-02;
   mol_mass(2)  = 2.80134e-02;
   mol_mass(3)  = 1.59994e-02;
   mol_mass(4)  = 1.40067e-02;
   mol_mass(5)  = 3.00061e-02;

%;....... Characteristic vibrational temperature
   T_vibr(1)  = 2273.6e+00;
   T_vibr(2)  = 3393.5e+00;
   T_vibr(3)  = 0.0e+00;
   T_vibr(4)  = 0.0e+00;
   T_vibr(5)  = 2739.7e+00;

%....... Heat of formation at 0 K
   heat_form_0K(1)  = 0.0e+00;
   heat_form_0K(2)  = 0.0e+00;
   heat_form_0K(3)  = 249336.e+00;
   heat_form_0K(4)  = 471136.e+00;
   heat_form_0K(5)  = 89836.2e+00;
   
   
   
%% Public Variables

Ru = 8.3143;

gam = 1.4;

eta_O2_std = 7.350994;
eta_N2_std = 27.30040;

T_ref = 288.15;
rho_ref = 1.225;
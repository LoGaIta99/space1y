%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% H.T.T.A. - A.Y. 2018-19 - prof. M. Guilizzoni                           %
% SOLUTION BY PDE TOOLBOX AND ANALYTICAL OF A 1D STEADY-STATE CONDUCTION  %
% PROBLEM: THREE LAYER SLAB WITH AN ACTIVE LAYER AND CONVECTIVE B.C.s     %
% (Exercise 1.7 of the collection of "blackboard exercises" v. 2018/19)   %
%                                                                         %
% BEFORE launching this file, the exercise has to be solved with          %
% PDE Toolbox (file PDETool_2019_Es_107.m) and mesh and solution exported %
% to the workspace                                                        %
% (variables: p e t for the mesh, u for the solution)                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%INPUT DATA

%domain dimensions
Lx=0.9;

%input temperatures or other b.c.s
Too1=293.15;
Too6=303.15;

%convective coefficients
h1=10;
h6=20;

%POSTPROCESSING OF THE NUMERICAL SOLUTION
nx=100;
ny=100;

% get boundaries of the mesh
xmin=min(p(1,:));
xmax=max(p(1,:));
ymin=min(p(2,:));
ymax=max(p(2,:));

% length step-sizes
dx=(xmax-xmin)/(nx-1);
dy=(ymax-ymin)/(ny-1);

x=xmin:dx:xmax;
y=ymin:dy:ymax;

% interpolate the triangular mesh (from PDEToolbox) into rectangular grid
uxy=tri2grid(p,t,u,x,y);


%ANALYTICAL SOLUTION

%INPUT DATA
sA=0.2;
sB=0.5;
sC=0.2;
lbdA=0.5;
lbdB=1;
lbdC=2;
Up3B=1e3;

% ANALYTICAL SOLUTION
Qp2totB=Up3B*sB;
RL=1/h1+sA/lbdA;
RR=sC/lbdC+1/h6;
T3=(Too6+Up3B*sB*RR+Too1*RR/RL+Up3B*sB^2/(2*lbdB)+Too1*sB/(RL*lbdB))/(sB/(lbdB*RL)+1+RR/RL);
C1B=(T3-Too1)/(RL*lbdB);
C2B=T3;
T4=-Up3B*sB^2/(2*lbdB)+C1B*sB+C2B;
Qp23=-(T3-Too1)/RL;
Qp24=-(Too6-T4)/RR;
x_where_Tmax=C1B*lbdB/Up3B;
Tmax=-Up3B*x_where_Tmax^2/(2*lbdB)+C1B*x_where_Tmax+C2B;


%PLOT OF THE ANALYTICAL AND NUMERICAL RESULTS

%TEMPERATURE - analytical
figure; hold on;

xA=0:0.01:sA;
xB=0:0.01:sB;
xC=0:0.01:sC;

TA=T3+Qp23*(sA-xA)./lbdA;
Qp2A=repmat(Qp23,length(xA),1);
TC=T4-Qp24*xC./lbdC;
Qp2C=repmat(Qp24,length(xC),1);
TB=-Up3B*xB.^2./(2*lbdB)+C1B*xB+C2B;
Qp2B=Up3B*xB-C1B*lbdB;

plot(xA,TA,'g');
plot(sA+xB,TB,'r');
plot(sA+sB+xC,TC,'b');

%numerical from PDEtool
plot(x,uxy(50,:),'b*');

title('T(x)');
xlabel('x [m]');
ylabel('T [K]');

%HEAT FLUX - analytical
figure; hold on;
plot(xA,Qp2A,'g');
plot(sA+xB,Qp2B,'r');
plot(sA+sB+xC,Qp2C,'b');
title('Qp2(x)');
xlabel('x [m]');
ylabel('Qp2(x) [W/m2]');
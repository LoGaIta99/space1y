Program written by Nicolas Lauzier in collaboration with Daniel Rousse, Université Laval, 2003-2004


-----------------------------

To use the program, you need:

vfact.fig
vfact.m
viewfactor.m
functionintegral.m
functionviewfactorarea.m

------------------------------

vfact is a simple GUI that can be use to simplify the utilisation of the function viewfactor.
It calculates view factors between surfaces which can be used later in radiation problems.

functionintegral.m and functionviewfactorarea.m are two functions used by viewfactor.m,
so you never need to use them directly.

viewfactor use the CDIF method to calculate view factors between planar polygons.
The polygons can be in any orientation and can have any shape. The only restriction
is that any of the two polygon must not cross the plane containing the other polygon.

------------------------------

To use the GUI, simply type "vfact" in the command window when you are in a directory containing
the required files. This will launch the GUI. After that, just follow the instructions on
the screen.

--------------------------------

Questions/comments: nlauzier@yahoo.com
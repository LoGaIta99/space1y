%Author: M. Guilizzoni, Department of Energy, Politecnico di Milano
%March 2013
%
%function to calculate view factors using the analytical equations
%for the examples 1 and 2 of the Tables 10.2 and 10.3 of the 
%textbook: Lienhard & Lienhard, A Heat Transfer Textbook, 3rd Ed.
%
%rect1 and rect2 are the two rectangles for which the view factors are
%calculated
%their (4) vertices must be given following the rule: each vertex is 
%described by a row vector; from the 1st to the 2nd gives the first 
%direction, from the 2nd to the 3rd gives the second direction.
%geom indicates the case in Table 10.3, 1 = case 1, etc.
%
%examples: see file "examples_analytical_view_factors.m"
%
function F12=calculate_view_factors_by_equation(rect1,rect2,geom)

switch(geom)
    case(1)
       a=rect1(2,1)-rect1(1,1);
       b=rect1(3,2)-rect1(2,2);
       c=rect2(1,3)-rect1(1,3);

       X=a/c;
       Y=b/c;

       temp1=((1+X^2)*(1+Y^2)/(1+X^2+Y^2))^0.5;
       temp2=-X*atan(X)-Y*atan(Y);
       temp3=X*(1+Y^2)^0.5*atan(X/(1+Y^2)^0.5);
       temp4=Y*(1+X^2)^0.5*atan(Y/(1+X^2)^0.5);

       F12=2/(pi*X*Y)*(log(temp1)+temp2+temp3+temp4);
       
    case(2)
       h=rect2(4,3)-rect2(1,3);
       l=rect2(3,2)-rect2(4,2);
       w=rect1(2,1)-rect1(1,1);

       H=h/l;
       W=w/l;

       temp1=W*atan(1/W)-(H^2+W^2)^0.5*atan(1/(H^2+W^2)^0.5)+H*atan(1/H);
       temp2=(1+W^2)*(1+H^2)/(1+W^2+H^2);
       temp3=W^2*(1+W^2+H^2)/((1+W^2)*(W^2+H^2));
       temp4=H^2*(1+H^2+W^2)/((1+H^2)*(H^2+W^2));

       F12=1/(pi*W)*(temp1+1/4*log(temp2*temp3^(W^2)*temp4^(H^2)));       
end

end
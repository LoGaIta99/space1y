% Equilibrio chimico
%
% modelli di gas:   1) PIG
%                   2) O2,N2,O,N,NO

% reazioni chimiche considerate:
%     -  O2 <-> O + O                  (1)
%     -  N2 <-> N + N                  (2)
%     -  N + O <-> NO                  (3)

%

model = input('Select gas model: 1) PIG; 2) eq. 5 specie  ');
var   = input('Select independent variables: 1) E,rho; 2)T,rho     ');

%model=2;
%var=2;

if var==1
    E = input('Input the value of total energy/unit volume: E =   [kg/ms^2]  ');
    rho = input('Input the value of density: rho =   [kg/m^3]  ');
end

if var==2
     T = input('Input the value of emperature: T =   [K]  ');
     rho = input('Input the value of density: rho =   [kg/m^3]  ');
end

air_phys_properties

rho_sp = zeros(1,5);


% calcolo proprietÓ all'equilibrio

if model==1
    if var==1
        [T,P,P_rho,P_E,e_sp,h_sp,s_speed,rho_sp]=ideal_gas_Erho(E,rho);
    end
    if var==2
        [E,P,P_rho,P_T,e_sp,h_sp,s_speed,rho_sp]=ideal_gas_Trho(T,rho);
    end
end

if model==2
    if var==1
        % independent variables E,rho
        % use PIG at low temperatures (T<200 K)
        if E<1.426e5
            [T,P,P_rho,P_E,e_sp,h_sp,s_speed,rho_sp]=ideal_gas_Erho(E,rho);
        else
            [T,P,P_rho,P_E,e_sp,h_sp,s_speed,entropy,rho_sp]=equil_Erho(E,rho);
        end
    end
    
    if var==2
        % independent variables T,rho
        % use PIG at low temperatures (T<200 K)
        if T<200.
            [E,P,P_rho,P_T,e_sp,h_sp,s_speed,rho_sp]=ideal_gas_Trho(T,rho); 
        else
            [E,P,P_rho,P_T,e_sp,h_sp,s_speed,entropy,rho_sp]=equil_Trho(T,rho); 
        end
    end
end

% vari ed eventuali plot o disp

%     disp('Condizioni all''equilibrio:   ')
%     disp('E = '); disp(E)
%     disp('P = '); disp(P)
%     disp('rho = '); disp(rho)
%     disp('T = '); disp(T)



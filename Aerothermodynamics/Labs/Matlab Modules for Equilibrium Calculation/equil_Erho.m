% Equilibrium calculation at given E,rho

function [T,P,P_rho,P_E,e_sp,h_sp,s_speed,entropy,rho_sp]=equil_Erho(E,rho)

air_phys_properties;
N_sp  = 5;
N_var = 6;

% Initialize unknown molar concentrations and temperature (assume Cv=713)

C_in_O2 = rho * eta_O2_std;
C_in_N2 = rho * eta_N2_std;

T = E/(rho*713.);

if( T<500 )
 C = [C_in_O2*(1-0.02) C_in_N2*(1-0.02) C_in_O2*0.01 C_in_O2*0.01 C_in_N2*0.02];
elseif( T>1500 )
 C = [C_in_O2*(1-0.2) C_in_N2*(1-0.2) C_in_O2*0.1 C_in_O2*0.1 C_in_N2*0.2];
else
 C = [C_in_O2*(1-0.04) C_in_N2*(1-0.04) C_in_O2*0.02 C_in_O2*0.02 C_in_N2*0.04];
end

% Initialize vector of unknowns

x = [sqrt(C(1)) sqrt(C(2)) C(3) C(4) C(5) T]';


% Absolute Newton-Raphson solution

errf_mx = 1e-06;
errx_mx = 1e-05;
Nit_mx  = 5000;

errf = 1.;
errx = 1.;
it = 1;

%............ Start loop

while (it < Nit_mx) && ( (errf > errf_mx ) || (errx > errx_mx) );

    [fn,fjac]=jacobian_Erho(x,E,rho);


    errf = 0.;
    for i = 1:N_var;
      errf = max( errf, abs( fn(i) ) );
    end

% ..... LU decomposition
    jac = fjac;
    [fjac,indx]= ludcmp(jac,N_var);

% ..... LU backsubstitution
          
    [rhs]=lubksn(fn,fjac,indx,N_var);

% ..... update of the unknowns
%    rhs = fn / fjac;

    errx = 0.;
    for i = 1:N_var;
      errx = max( errx, abs( rhs(i) ) );
      x(i) = abs( x(i) + rhs(i) );
    end

    it = it + 1;

end
%........ end loop


% ---------- Gas properties evaluation
      zero = 0.;
      two  = 2.;
% ..... jacobian matrix evaluation
      [~,fjac]=jacobian_Erho(x,E,rho);

% ..... system partial derivatives versus E at costant rho (ds_E)
      ds_E = [0 0 0 0 0 1./E];
% ..... system partial derivatives versus rho at costant E (ds_rho)
      ds_rho = [0 0 0 two*eta_O2_std two*eta_N2_std 0];

% ... LU decomposition
      jac = fjac;
      [fjac,indx]= ludcmp(jac,N_var);

% ... LU backsubstitution
% ... partial derivatives of (rho/mol_mass) and T versus E at costant rho
      [rhs]=lubksn(ds_E,fjac,indx,N_var);
%      rhs = ds_E / fjac;
      
      ds_E    = rhs;
      ds_E(1) = two * x(1) * ds_E(1);
      ds_E(2) = two * x(2) * ds_E(2);

% ... partial derivatives of (rho/mol_mass) and T versus rho at costant E
      [rhs]=lubksn(ds_rho,fjac,indx,N_var);
%      rhs = ds_rho / fjac;

      ds_rho    = rhs;
      ds_rho(1) = two * x(1) * ds_rho(1);
      ds_rho(2) = two * x(2) * ds_rho(2);

% ... extract species partial densities and temperature
      rho_sp = [0 0 0 0 0];
      x(1) = x(1)^2;
      x(2) = x(2)^2;
      for i = 1:N_sp;
         rho_sp(i) = mol_mass(i) * x(i);
      end
% writing
      mass_frac = rho_sp / rho
      T = x(N_var)

% ... compute partial derivatives of pressure (P_E, P_rho), mixture specific constant (R),
%                   pressure (P), mixture enthalpy (ent), mixture speed of sound (c),
%                   mixture specific entropy
      entropy     = zero;
      mms_sp      = [0 0 0 0 0];
      par1        = log( T/T_ref );
      sum_RoverM  = zero;
      sum_dsp_E   = zero;
      sum_dsp_rho = zero;
      for i = 1:N_sp;
         sum_RoverM  = sum_RoverM + x(i);
         sum_dsp_E   = sum_dsp_E + ds_E(i);
         sum_dsp_rho = sum_dsp_rho + ds_rho(i);

         mms_sp(i) = -log( rho_sp(i)/rho_ref );
         bi   =  1.5;
         if( (i <= 2) || (i == 5) ); 
            bi   = 2.5;
            exp0 = exp( -T_vibr(i)/T_ref ); 
            exp1 = exp( T_vibr(i)/T );
        mms_sp(i) =  mms_sp(i) + log((1.-exp0)/(1.-1./exp1)) + (T_vibr(i)/T)/(exp1-1.);
         end
         mms_sp(i) = ( mms_sp(i) + bi * par1 ) * Ru;
         entropy   = entropy  + x(i) * mms_sp(i);
      end
      entropy = entropy / rho;
     
      R       = sum_RoverM * Ru / rho
      P       = rho * R * T
      e_sp    = E / rho;
      h_sp    = e_sp + R * T;
      P_E     = ds_E(N_var) * rho * R + T * Ru * sum_dsp_E;
      P_rho   = ds_rho(N_var) * rho * R + T * Ru * sum_dsp_rho;
      s_speed = sqrt( P_rho + P_E * h_sp )


return
%Graphic program that calculates view factors
%-------------------------------------------------
%Graphic interface for the functionviewfactor
%------------------------------------------------------------------------------------------
%Developped by Nicolas Lauzier in collaboration with Daniel Rousse, Université Laval, 2003.
%------------------------------------------------------------------------------------------
%This program calculates view factors between any two planar surfaces. 
%You only have to enter coordinates of the vertices that set the outline 
%of both figures and to enter the desired number of significant digits. 
%Keep in mind that a great number of significant digits means a lot of 
%calculations so it can really affect the amount of time that the computer 
%takes to find the results.

function varargout = vfact(varargin)


%Procedure to launch the graphical user interface
if nargin == 0  % LAUNCH GUI

	fig = openfig(mfilename,'reuse');

	% Generate a structure of handles to pass to callbacks, and store it. 
	handles = guihandles(fig);
	guidata(fig, handles);

	if nargout > 0
		varargout{1} = fig;
	end

elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK

	try
		[varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
	catch
		disp(lasterr);
	end

end

% --------------------------------------------------------------------
function varargout = Calcul_Callback(h, eventdata, handles, varargin)
%Function to execute when the user press the button "Calculate"

%Labels show 'Wait a moment please' when the computer calculates.
set(handles.resultats,'string','Wait a moment please');
set(handles.resultats2,'string','Wait a moment please');
set(handles.aire1,'string','Wait a moment please');
set(handles.aire2,'string','Wait a moment please');


%Look for the coordinates of figure 1
figure1(1,1)=str2num(get(handles.figure1X1,'string'));
figure1(1,2)=str2num(get(handles.figure1Y1,'string'));
figure1(1,3)=str2num(get(handles.figure1Z1,'string'));

figure1(2,1)=str2num(get(handles.figure1X2,'string'));
figure1(2,2)=str2num(get(handles.figure1Y2,'string'));
figure1(2,3)=str2num(get(handles.figure1Z2,'string'));

figure1(3,1)=str2num(get(handles.figure1X3,'string'));
figure1(3,2)=str2num(get(handles.figure1Y3,'string'));
figure1(3,3)=str2num(get(handles.figure1Z3,'string'));

%When we reach the fourth point, we look if the box is empty or filled before we continue.
if isempty(get(handles.figure1X4,'string'))==0 & isempty(get(handles.figure1Y4,'string'))==0 & isempty(get(handles.figure1Z4,'string'))==0
figure1(4,1)=str2num(get(handles.figure1X4,'string'));
figure1(4,2)=str2num(get(handles.figure1Y4,'string'));
figure1(4,3)=str2num(get(handles.figure1Z4,'string'));
end
if isempty(get(handles.figure1X5,'string'))==0 & isempty(get(handles.figure1Y5,'string'))==0 & isempty(get(handles.figure1Z5,'string'))==0
figure1(5,1)=str2num(get(handles.figure1X5,'string'));
figure1(5,2)=str2num(get(handles.figure1Y5,'string'));
figure1(5,3)=str2num(get(handles.figure1Z5,'string'));
end
if isempty(get(handles.figure1X6,'string'))==0 & isempty(get(handles.figure1Y6,'string'))==0 & isempty(get(handles.figure1Z6,'string'))==0
figure1(6,1)=str2num(get(handles.figure1X6,'string'));
figure1(6,2)=str2num(get(handles.figure1Y6,'string'));
figure1(6,3)=str2num(get(handles.figure1Z6,'string'));
end
if isempty(get(handles.figure1X7,'string'))==0 & isempty(get(handles.figure1Y7,'string'))==0 & isempty(get(handles.figure1Z7,'string'))==0
figure1(7,1)=str2num(get(handles.figure1X7,'string'));
figure1(7,2)=str2num(get(handles.figure1Y7,'string'));
figure1(7,3)=str2num(get(handles.figure1Z7,'string'));
end
if isempty(get(handles.figure1X8,'string'))==0 & isempty(get(handles.figure1Y8,'string'))==0 & isempty(get(handles.figure1Z8,'string'))==0
figure1(8,1)=str2num(get(handles.figure1X8,'string'));
figure1(8,2)=str2num(get(handles.figure1Y8,'string'));
figure1(8,3)=str2num(get(handles.figure1Z8,'string'));
end
if isempty(get(handles.figure1X9,'string'))==0 & isempty(get(handles.figure1Y9,'string'))==0 & isempty(get(handles.figure1Z9,'string'))==0
figure1(9,1)=str2num(get(handles.figure1X9,'string'));
figure1(9,2)=str2num(get(handles.figure1Y9,'string'));
figure1(9,3)=str2num(get(handles.figure1Z9,'string'));
end
if isempty(get(handles.figure1X10,'string'))==0 & isempty(get(handles.figure1Y10,'string'))==0 & isempty(get(handles.figure1Z10,'string'))==0
figure1(10,1)=str2num(get(handles.figure1X10,'string'));
figure1(10,2)=str2num(get(handles.figure1Y10,'string'));
figure1(10,3)=str2num(get(handles.figure1Z10,'string'));
end
if isempty(get(handles.figure1X11,'string'))==0 & isempty(get(handles.figure1Y11,'string'))==0 & isempty(get(handles.figure1Z11,'string'))==0
figure1(11,1)=str2num(get(handles.figure1X11,'string'));
figure1(11,2)=str2num(get(handles.figure1Y11,'string'));
figure1(11,3)=str2num(get(handles.figure1Z11,'string'));
end
if isempty(get(handles.figure1X12,'string'))==0 & isempty(get(handles.figure1Y12,'string'))==0 & isempty(get(handles.figure1Z12,'string'))==0
figure1(12,1)=str2num(get(handles.figure1X12,'string'));
figure1(12,2)=str2num(get(handles.figure1Y12,'string'));
figure1(12,3)=str2num(get(handles.figure1Z12,'string'));
end
if isempty(get(handles.figure1X13,'string'))==0 & isempty(get(handles.figure1Y13,'string'))==0 & isempty(get(handles.figure1Z13,'string'))==0
figure1(13,1)=str2num(get(handles.figure1X13,'string'));
figure1(13,2)=str2num(get(handles.figure1Y13,'string'));
figure1(13,3)=str2num(get(handles.figure1Z13,'string'));
end

%Look for the coordinates of figure 2
figure2(1,1)=str2num(get(handles.figure2X1,'string'));
figure2(1,2)=str2num(get(handles.figure2Y1,'string'));
figure2(1,3)=str2num(get(handles.figure2Z1,'string'));

figure2(2,1)=str2num(get(handles.figure2X2,'string'));
figure2(2,2)=str2num(get(handles.figure2Y2,'string'));
figure2(2,3)=str2num(get(handles.figure2Z2,'string'));

figure2(3,1)=str2num(get(handles.figure2X3,'string'));
figure2(3,2)=str2num(get(handles.figure2Y3,'string'));
figure2(3,3)=str2num(get(handles.figure2Z3,'string'));

%When we reach the fourth point, we look if the box is empty or filled before we continue.
if isempty(get(handles.figure2X4,'string'))==0 & isempty(get(handles.figure2Y4,'string'))==0 & isempty(get(handles.figure2Z4,'string'))==0
figure2(4,1)=str2num(get(handles.figure2X4,'string'));
figure2(4,2)=str2num(get(handles.figure2Y4,'string'));
figure2(4,3)=str2num(get(handles.figure2Z4,'string'));
end
if isempty(get(handles.figure2X5,'string'))==0 & isempty(get(handles.figure2Y5,'string'))==0 & isempty(get(handles.figure2Z5,'string'))==0
figure2(5,1)=str2num(get(handles.figure2X5,'string'));
figure2(5,2)=str2num(get(handles.figure2Y5,'string'));
figure2(5,3)=str2num(get(handles.figure2Z5,'string'));
end
if isempty(get(handles.figure2X6,'string'))==0 & isempty(get(handles.figure2Y6,'string'))==0 & isempty(get(handles.figure2Z6,'string'))==0
figure2(6,1)=str2num(get(handles.figure2X6,'string'));
figure2(6,2)=str2num(get(handles.figure2Y6,'string'));
figure2(6,3)=str2num(get(handles.figure2Z6,'string'));
end
if isempty(get(handles.figure2X7,'string'))==0 & isempty(get(handles.figure2Y7,'string'))==0 & isempty(get(handles.figure2Z7,'string'))==0
figure2(7,1)=str2num(get(handles.figure2X7,'string'));
figure2(7,2)=str2num(get(handles.figure2Y7,'string'));
figure2(7,3)=str2num(get(handles.figure2Z7,'string'));
end
if isempty(get(handles.figure2X8,'string'))==0 & isempty(get(handles.figure2Y8,'string'))==0 & isempty(get(handles.figure2Z8,'string'))==0
figure2(8,1)=str2num(get(handles.figure2X8,'string'));
figure2(8,2)=str2num(get(handles.figure2Y8,'string'));
figure2(8,3)=str2num(get(handles.figure2Z8,'string'));
end
if isempty(get(handles.figure2X9,'string'))==0 & isempty(get(handles.figure2Y9,'string'))==0 & isempty(get(handles.figure2Z9,'string'))==0
figure2(9,1)=str2num(get(handles.figure2X9,'string'));
figure2(9,2)=str2num(get(handles.figure2Y9,'string'));
figure2(9,3)=str2num(get(handles.figure2Z9,'string'));
end
if isempty(get(handles.figure2X10,'string'))==0 & isempty(get(handles.figure2Y10,'string'))==0 & isempty(get(handles.figure2Z10,'string'))==0
figure2(10,1)=str2num(get(handles.figure2X10,'string'));
figure2(10,2)=str2num(get(handles.figure2Y10,'string'));
figure2(10,3)=str2num(get(handles.figure2Z10,'string'));
end
if isempty(get(handles.figure2X11,'string'))==0 & isempty(get(handles.figure2Y11,'string'))==0 & isempty(get(handles.figure2Z11,'string'))==0
figure2(11,1)=str2num(get(handles.figure2X11,'string'));
figure2(11,2)=str2num(get(handles.figure2Y11,'string'));
figure2(11,3)=str2num(get(handles.figure2Z11,'string'));
end
if isempty(get(handles.figure2X12,'string'))==0 & isempty(get(handles.figure2Y12,'string'))==0 & isempty(get(handles.figure2Z12,'string'))==0
figure2(12,1)=str2num(get(handles.figure2X12,'string'));
figure2(12,2)=str2num(get(handles.figure2Y12,'string'));
figure2(12,3)=str2num(get(handles.figure2Z12,'string'));
end
if isempty(get(handles.figure2X13,'string'))==0 & isempty(get(handles.figure2Y13,'string'))==0 & isempty(get(handles.figure2Z13,'string'))==0
figure2(13,1)=str2num(get(handles.figure2X13,'string'))
figure2(13,2)=str2num(get(handles.figure2Y13,'string'))
figure2(13,3)=str2num(get(handles.figure2Z13,'string'))
end

%What's the number of significant digits?
prec=str2num(get(handles.nbchiffres,'string'));


%We calculate view factors and areas of both figures
[facteur,facteur21,aire1,aire2]=viewfactor(figure1,figure2,prec);
  


%We show the results
set(handles.resultats,'string',num2str(facteur,prec));
set(handles.resultats2,'string',num2str(facteur21,prec));
set(handles.aire1,'string',num2str(aire1,prec));
set(handles.aire2,'string',num2str(aire2,prec));


%We draw the figures
xmin=min(min(figure1(:,1)),min(figure2(:,1)));
xmax=max(max(figure1(:,1)),max(figure2(:,1)));
ymin=min(min(figure1(:,2)),min(figure2(:,2)));
ymax=max(max(figure1(:,2)),max(figure2(:,2)));
zmin=min(min(figure1(:,3)),min(figure2(:,3)));
zmax=max(max(figure1(:,3)),max(figure2(:,3)));
intx=xmax-xmin;
inty=ymax-ymin;
intz=zmax-zmin;
intmax=max([intx,inty,intz]);

fill3(figure1(:,1),figure1(:,2),figure1(:,3),'r',figure2(:,1),figure2(:,2),figure2(:,3),'b')
set(gca,'xlim',[xmin-(intmax-intx)/2,xmax+(intmax-intx)/2])
set(gca,'ylim',[ymin-(intmax-inty)/2,ymax+(intmax-inty)/2])
set(gca,'zlim',[zmin-(intmax-intz)/2,zmax+(intmax-intz)/2])

%We put titles on axes
xlabel('x')
ylabel('y')
zlabel('z')

%We show the grid and allow the rotation of graphic
ROTATE3D ON
GRID ON

% --------------------------------------------------------------------
function varargout = recommencer_Callback(h, eventdata, handles, varargin)
%This function is activated when the user press the Restart button

%We empty the grid for figure 1

set(handles.figure1X1,'string','');
set(handles.figure1Y1,'string','');
set(handles.figure1Z1,'string','');

set(handles.figure1X2,'string','');
set(handles.figure1Y2,'string','');
set(handles.figure1Z2,'string','');

set(handles.figure1X3,'string','');
set(handles.figure1Y3,'string','');
set(handles.figure1Z3,'string','');

set(handles.figure1X4,'string','');
set(handles.figure1Y4,'string','');
set(handles.figure1Z4,'string','');

set(handles.figure1X5,'string','');
set(handles.figure1Y5,'string','');
set(handles.figure1Z5,'string','');

set(handles.figure1X6,'string','');
set(handles.figure1Y6,'string','');
set(handles.figure1Z6,'string','');

set(handles.figure1X7,'string','');
set(handles.figure1Y7,'string','');
set(handles.figure1Z7,'string','');

set(handles.figure1X8,'string','');
set(handles.figure1Y8,'string','');
set(handles.figure1Z8,'string','');

set(handles.figure1X9,'string','');
set(handles.figure1Y9,'string','');
set(handles.figure1Z9,'string','');

set(handles.figure1X10,'string','');
set(handles.figure1Y10,'string','');
set(handles.figure1Z10,'string','');

set(handles.figure1X11,'string','');
set(handles.figure1Y11,'string','');
set(handles.figure1Z11,'string','');

set(handles.figure1X12,'string','');
set(handles.figure1Y12,'string','');
set(handles.figure1Z12,'string','');

set(handles.figure1X13,'string','');
set(handles.figure1Y13,'string','');
set(handles.figure1Z13,'string','');

%We empty the grid for figure 2

set(handles.figure2X1,'string','');
set(handles.figure2Y1,'string','');
set(handles.figure2Z1,'string','');

set(handles.figure2X2,'string','');
set(handles.figure2Y2,'string','');
set(handles.figure2Z2,'string','');

set(handles.figure2X3,'string','');
set(handles.figure2Y3,'string','');
set(handles.figure2Z3,'string','');

set(handles.figure2X4,'string','');
set(handles.figure2Y4,'string','');
set(handles.figure2Z4,'string','');

set(handles.figure2X5,'string','');
set(handles.figure2Y5,'string','');
set(handles.figure2Z5,'string','');

set(handles.figure2X6,'string','');
set(handles.figure2Y6,'string','');
set(handles.figure2Z6,'string','');

set(handles.figure2X7,'string','');
set(handles.figure2Y7,'string','');
set(handles.figure2Z7,'string','');

set(handles.figure2X8,'string','');
set(handles.figure2Y8,'string','');
set(handles.figure2Z8,'string','');

set(handles.figure2X9,'string','');
set(handles.figure2Y9,'string','');
set(handles.figure2Z9,'string','');

set(handles.figure2X10,'string','');
set(handles.figure2Y10,'string','');
set(handles.figure2Z10,'string','');

set(handles.figure2X11,'string','');
set(handles.figure2Y11,'string','');
set(handles.figure2Z11,'string','');

set(handles.figure2X12,'string','');
set(handles.figure2Y12,'string','');
set(handles.figure2Z12,'string','');

set(handles.figure2X13,'string','');
set(handles.figure2Y13,'string','');
set(handles.figure2Z13,'string','');

%We erase the graphic, we remove the grid and we block the 3D rotation
cla
GRID OFF
ROTATE3D OFF

%We center the view
set(gca,'view',[0 90])

%We remove the results showed
set(handles.resultats,'string','-');
set(handles.resultats2,'string','-');
set(handles.aire1,'string','-');
set(handles.aire2,'string','-');

%We erase the titles
xlabel('')
ylabel('')
zlabel('')

%We erase the axes graduation
set(gca,'XTickLabel','')
set(gca,'YTickLabel','')

